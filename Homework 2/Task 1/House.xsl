<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:h="HouseChema"
    xmlns:i="HouseInfo" xmlns:r="RoomsChema" xmlns:xalan="http://xml.apache.org/xslt"
    exclude-result-prefixes="h i r xalan">
    <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" xalan:indent-amount="4"/>

    <xsl:variable name="lc" select="'abcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="uc" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

    <!-- создаём корневой элемент, применяем шаблон с сортировками -->
    <xsl:template match="/">
        <AllRooms>
            <xsl:apply-templates select="descendant::r:Room">
                <xsl:sort select="ancestor::h:House/@City"/>
                <xsl:sort
                    select="translate((ancestor::h:House/i:Address/text()), concat($lc, $uc, ' '), '')"
                    data-type="number"/>
                <xsl:sort select="ancestor::h:Block/@number"/>
                <xsl:sort select="./@nuber" data-type="number"/>
            </xsl:apply-templates>
        </AllRooms>
    </xsl:template>

    <xsl:template match="r:Room">
        <!-- переводим название города в UPPERCASE -->
        <xsl:variable name="CITY" select="translate(ancestor::h:House/@City, $lc, $uc)"/>
        <!-- вычисляем число комнат в доме -->
        <xsl:variable name="HouseRoomsCount" select="count(../../../descendant::r:Room)"/>
        <!-- вычисляем число гостей в доме -->
        <xsl:variable name="HouseGuestsCount" select="sum(../../../descendant::r:Room/@guests)"/>
        <!-- вычисляем число комнат в блоке -->
        <xsl:variable name="BlockRoomsCount" select="count(../descendant::r:Room)"/>

        <Room>
            <Address>
                <xsl:value-of
                    select="concat($CITY, '/', ancestor::h:House/i:Address, '/', ancestor::h:Block/@number, '/', ./@nuber)"
                />
            </Address>
            <HouseRoomsCount>
                <xsl:value-of select="$HouseRoomsCount"/>
            </HouseRoomsCount>
            <BlockRoomsCount>
                <xsl:value-of select="$BlockRoomsCount"/>
            </BlockRoomsCount>
            <HouseGuestsCount>
                <xsl:value-of select="$HouseGuestsCount"/>
            </HouseGuestsCount>
            <GuestsPerRoomAverage>
                <xsl:value-of select="floor($HouseGuestsCount div $HouseRoomsCount)"/>
            </GuestsPerRoomAverage>
            <!-- 
            <Allocated Single="{@guests=1}" Double="{@guests=2}" Triple="{@guests=3}" Quarter="{@guests=4}"/>
            При трансформации через Xalan атрибуты выводятся в обратном порядке, при трансформации через Saxon всё нормально.
            Пришлось делать через <xsl:attribute>. В папке прилагаю скриншот окна 1.png. -->
            <Allocated>
                <xsl:attribute name="Single">
                    <xsl:value-of select="@guests = 1"/>
                </xsl:attribute>
                <xsl:attribute name="Double">
                    <xsl:value-of select="@guests = 2"/>
                </xsl:attribute>
                <xsl:attribute name="Triple">
                    <xsl:value-of select="@guests = 3"/>
                </xsl:attribute>
                <xsl:attribute name="Quarter">
                    <xsl:value-of select="@guests = 4"/>
                </xsl:attribute>
            </Allocated>
        </Room>
    </xsl:template>
</xsl:stylesheet>
