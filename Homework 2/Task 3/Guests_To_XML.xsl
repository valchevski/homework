<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns="xml_version_1.0" xmlns:d="xml_version_1.0"
    xmlns:end="https://www.meme-arsenal.com/create/template/43024"
    xmlns:xalan="http://xml.apache.org/xslt"
    exclude-result-prefixes="d xalan">
    <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" xalan:indent-amount="4"/>

    <xsl:variable name="defNS">xml_version_1.0</xsl:variable>

    <xsl:template match="/">
        <Peoples>
            <xsl:apply-templates select="d:Guests/d:Guest | d:Guests/end:Guest"/>
        </Peoples>
    </xsl:template>

    <xsl:template match="d:Guests/d:Guest">
        <People AGE="{@Age}" NATIONALTY="{@Nationalty}" GENDER="{@Gender}" NAME="{@Name}">
            <xsl:call-template name="addInfo"/>
        </People>
    </xsl:template>
    
    <xsl:template match="d:Guests/end:Guest">
        <end:People AGE="{@Age}" NATIONALTY="{@Nationalty}" GENDER="{@Gender}" NAME="{@Name}">
            <xsl:call-template name="addInfo"/>
        </end:People>
    </xsl:template>
    
    <xsl:template name="addInfo">
        <Type>
            <Text>
                <xsl:value-of select="./d:Type/text()"/>
            </Text>
        </Type>
        <Profile>
            <Address>
                <Text>
                    <xsl:value-of select="./d:Profile/d:Address/text()"/>
                </Text>
            </Address>
            <xsl:if test="./d:Profile/d:Email/text()">
                <Email>
                    <Text>
                        <xsl:value-of select="./d:Profile/d:Email/text()"/>
                    </Text>
                </Email>
            </xsl:if>
        </Profile>
    </xsl:template>
</xsl:stylesheet>
