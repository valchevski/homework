<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:d="xml_version_1.0"
    xmlns:end="https://www.meme-arsenal.com/create/template/43024">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:element name="Guests">
            <xsl:apply-templates select="*/d:Guest|*/end:Guest"></xsl:apply-templates>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="d:Guest|end:Guest">
            <xsl:for-each select=".">
                <xsl:value-of
                    select="concat(namespace-uri(.), '|', @Age, '/', @Nationalty, '/', @Gender, '/', @Name, '/', ./d:Type/text(), '/', ./d:Profile/d:Address/text(), '/', ./d:Profile/d:Email/text(), '|')"/>
            </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
