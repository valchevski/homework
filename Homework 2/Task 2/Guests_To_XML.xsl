<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns="xml_version_1.0" xmlns:end="https://www.meme-arsenal.com/create/template/43024"
    xmlns:xalan="http://xml.apache.org/xslt" exclude-result-prefixes="xalan">
    <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" xalan:indent-amount="4"/>

    <xsl:variable name="defaultNS">xml_version_1.0</xsl:variable>
    <xsl:variable name="endNS">https://www.meme-arsenal.com/create/template/43024</xsl:variable>

    <xsl:template match="/">
        <Guests>
            <xsl:call-template name="splitPersonData"/>
        </Guests>
    </xsl:template>

    <xsl:template name="splitPersonData">
        <xsl:param name="splitText" select="."/>

        <xsl:if test="string-length($splitText)">
            <xsl:variable name="gNSpace"
                select="substring-before($splitText, '|')"/>
            
            <!-- в зависимости от namespace создаем соответствующие ноды -->
            <xsl:choose>
                <xsl:when test="$gNSpace = $defaultNS">
                    <Guest>
                        <xsl:call-template name="addInfo">
                            <xsl:with-param name="splitText">
                                <xsl:value-of select="substring-after($splitText, '|')"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </Guest>
                </xsl:when>
                <xsl:when test="$gNSpace = $endNS">
                    <end:Guest>
                        <xsl:call-template name="addInfo">
                            <xsl:with-param name="splitText">
                                <xsl:value-of select="substring-after($splitText, '|')"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </end:Guest>
                </xsl:when>
            </xsl:choose>

            <!-- переходим к следующему гостю -->
            <xsl:call-template name="splitPersonData">
                <xsl:with-param name="splitText">
                    <xsl:value-of select="substring-after(substring-after($splitText, '|'),'|')"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="addInfo">
        <xsl:param name="splitText"/>   
        <xsl:variable name="gAge" select="substring-before($splitText, '/')"/>
        <xsl:variable name="gNation"
            select="substring-before(substring-after($splitText, concat($gAge, '/')), '/')"/>
        <xsl:variable name="gGender"
            select="substring-before(substring-after($splitText, concat('/', $gNation, '/')), '/')"/>
        <xsl:variable name="gName"
            select="substring-before(substring-after($splitText, concat('/', $gGender, '/')), '/')"/>
        <xsl:variable name="gType"
            select="substring-before(substring-after($splitText, concat('/', $gName, '/')), '/')"/>
        <xsl:variable name="gAdd"
            select="substring-before(substring-after($splitText, concat('/', $gType, '/')), '/')"/>
        <xsl:variable name="gEmail"
            select="substring-before(substring-after($splitText, concat('/', $gAdd, '/')), '|')"/>
        
        <xsl:attribute name="Age">
            <xsl:value-of select="$gAge"/>
        </xsl:attribute>
        <xsl:attribute name="Nationalty">
            <xsl:value-of select="$gNation"/>
        </xsl:attribute>
        <xsl:attribute name="Gender">
            <xsl:value-of select="$gGender"/>
        </xsl:attribute>
        <xsl:attribute name="Name">
            <xsl:value-of select="$gName"/>
        </xsl:attribute>
        <Type>
            <xsl:value-of select="$gType"/>
        </Type>
        <Profile>
            <Address>
                <xsl:value-of select="$gAdd"/>
            </Address>
            <xsl:if test="$gEmail">
                <Email>
                    <xsl:value-of select="$gEmail"/>
                </Email>
            </xsl:if>
        </Profile>
    </xsl:template>
</xsl:stylesheet>
