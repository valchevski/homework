<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:d="xml_version_1.0"
    xmlns:end="https://www.meme-arsenal.com/create/template/43024">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>

    <!-- переменные для lower-case -->
    <xsl:variable name="lc" select="'abcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="uc" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

    <xsl:template match="d:Guests">
        <xsl:element name="result">
            <xsl:value-of select="'&#xA;'"/>
            <xsl:apply-templates select="d:Guest"/>
            <xsl:apply-templates select="end:Guest"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="d:Guest|end:Guest">
        <xsl:if test="./@Name = 'Jimmy'">
            <!-- имена до Jimmy в нижнем регистре -->
            <xsl:for-each select="preceding-sibling::*">
                <xsl:value-of select="concat(translate(./@Name, $uc, $lc), '&#xA;')"/>
            </xsl:for-each>
            <xsl:value-of select="concat('__________________________', '&#xA;')"/>

            <!-- адреса не белорусов после Jimmy -->
            <xsl:for-each select="following-sibling::*">
                <xsl:if test="not(./@Nationalty = 'BY')">
                    <xsl:value-of select="concat(./d:Profile/d:Address/text(), '&#xA;')"/>
                </xsl:if>
            </xsl:for-each>

            <xsl:value-of select="concat('__________________________', '&#xA;')"/>
        </xsl:if>
        
        <!-- элементы адресов не на Пушкинской с атрибутами -->
        <xsl:for-each select="./d:Profile/d:Address[not(contains(text(),'Pushkinskaya'))]">
            <xsl:element name="Address">
                <xsl:attribute name="Name">
                    <xsl:value-of select="../../@Name"/>
                </xsl:attribute>
                <xsl:attribute name="Nationaly">
                    <xsl:value-of select="../../@Nationalty"/>
                </xsl:attribute>
                <xsl:value-of select="./text()"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
