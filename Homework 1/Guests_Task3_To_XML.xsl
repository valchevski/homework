<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:element name="Guests">
            <xsl:apply-templates select="Guests"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="Guests">
        <xsl:call-template name="splitPersonData"/>
    </xsl:template>
    
    <xsl:template match="text()" name="splitPersonData">
        <xsl:param name="splitText" select="."/>
        
        <xsl:if test="string-length($splitText)">
            
            <!-- splitting data of one guest into variables  -->
            <xsl:variable name="gAge" select="substring-before($splitText, '/')"/>
            <xsl:variable name="gNation"
                select="substring-before(substring-after($splitText, concat($gAge, '/')), '/')"/>
            <xsl:variable name="gGender"
                select="substring-before(substring-after($splitText, concat($gNation, '/')), '/')"/>
            <xsl:variable name="gName"
                select="substring-before(substring-after($splitText, concat($gGender, '/')), '/')"/>
            <xsl:variable name="gType"
                select="substring-before(substring-after($splitText, concat($gName, '/')), '/')"/>
            <xsl:variable name="gAdd"
                select="substring-before(substring-after($splitText, concat($gType, '/')), '|')"/>
            
            <xsl:element name="Guest">
                <!-- creating attributes -->
                <xsl:attribute name="Age">
                    <xsl:value-of select="$gAge"/>
                </xsl:attribute>
                <xsl:attribute name="Nationalty">
                    <xsl:value-of select="$gNation"/>
                </xsl:attribute>
                <xsl:attribute name="Gender">
                    <xsl:value-of select="$gGender"/>
                </xsl:attribute>
                <xsl:attribute name="Name">
                    <xsl:value-of select="$gName"/>
                </xsl:attribute>
            
                <!-- creating nodes -->
                <xsl:element name="Type">
                    <xsl:value-of select="$gType"/>
                </xsl:element>
                
                <xsl:element name="Profile">
                    <xsl:element name="Address">
                        <xsl:value-of select="$gAdd"/>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
            
            <!-- go to the next guest -->
            <xsl:call-template name="splitPersonData">
                <xsl:with-param name="splitText">
                    <xsl:value-of select="substring-after($splitText, '|')"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>