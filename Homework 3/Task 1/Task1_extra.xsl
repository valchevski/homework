<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:xalan="http://xml.apache.org/xslt" exclude-result-prefixes=" xalan">
    <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" xalan:indent-amount="4"/>
    <xsl:key name="person" match="item" use="substring(@Name, 1, 1)"/>
    
    <!-- combine elements regardless of whether the letter is uppercase or lowercase;
         brings names to the correct form  -->
    
    <xsl:variable name="lc" select="'абвгдеёжзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="uc" select="'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
    
    <xsl:template match="list">
        <list>
            <xsl:apply-templates
                select="item[generate-id(.) = generate-id(key('person', translate(substring(@Name, 1, 1),$lc,$uc)) |
                key('person', translate(substring(@Name, 1, 1),$uc,$lc)))]">
                <xsl:sort select="@Name"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>
    
    <xsl:template match="item">
        <capital value="{translate(substring(@Name,1,1),$lc,$uc)}">
            <xsl:for-each select="key('person', translate(substring(@Name, 1, 1),$lc,$uc)) |
                key('person', translate(substring(@Name, 1, 1),$uc,$lc))">
                <xsl:sort select="@Name"/>
                <name>
                    <xsl:value-of select="concat(translate(substring(@Name,1,1),$lc,$uc),translate(substring(@Name,2),$uc,$lc))"/>
                </name>
            </xsl:for-each>
        </capital>
    </xsl:template>
</xsl:stylesheet>
